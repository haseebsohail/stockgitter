import React, { Component } from "react";
import {
    View,
    Modal,
    ActivityIndicator
} from "react-native";
import TextInputComponent from "../TextInput";
import ButtonComponent from "../Button";
import { TouchableOpacity } from "react-native-gesture-handler";
//Make a component
class ModalComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reason: ''
        }
    }
    handleInput = (value, flag) => {

        if (flag === "reject") {
            this.setState({ reason: value });
        }


    }
    callFunction() {

        if (this.state.reason.length > 0) {
            this.props.closeModal(this.state.reason)
        }
        else {
            alert("Please enter a reason");
        }
    }

    render() {
        return (
            <View>
                <Modal
                    transparent={true}
                    style={styles.modalStyle}
                    visible={this.props.show}
                    onRequestClose={() => {
                    }}>
                    <View style={styles.modalViewStyle}>

                        <View style={styles.ViewContainer}>
                            <TextInputComponent label={"Enter reason for Rejection"} flag={"reject"} handleInput={this.handleInput} />
                            <TouchableOpacity onPress={() => this.callFunction()}>

                                <ButtonComponent label={"Submit"} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.closeModal("")}>
                                <ButtonComponent label={"Cancel"} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </Modal>
            </View>
        );
    }
}
const styles = {
    modalStyle: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    modalViewStyle: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.8)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    ViewContainer: {
        backgroundColor: 'white',
        padding: 20,
        justifyContent: 'center',

    }
};
export default ModalComponent;