import React, { Component } from "react";
import {
    View,
    Text,
    Dimensions,

} from "react-native";
import { Container, Header, Content, Picker, Form, DatePicker } from "native-base";
import { country } from "../../utils/CountryConstants";
import { experienceArr } from "../../utils/experienceConstants";
//Make a component
class DatePickerComponent extends Component {

    constructor(props) {
        super(props);
        this.state = { chosenDate: new Date() };
        this.setDate = this.setDate.bind(this);
    }

    setDate(newDate) {
        this.setState({ chosenDate: newDate });
        this.props.handleDateofBirth(newDate);
    }
    render() {
        return (
            <View style={styles.pickerContainer}>
                <DatePicker
                    defaultDate={new Date()}
                    minimumDate={new Date(1900, 1, 1)}
                    maximumDate={new Date()}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"slide"}
                    androidMode={"default"}
                    placeHolderText="Date of Birth"
                    textStyle={{ color: "#000", marginLeft: 0, paddingLeft: 0 }}
                    placeHolderTextStyle={{ color: "#000", marginLeft: 0, paddingLeft: 0 }}
                    onDateChange={this.setDate}
                    disabled={false}
                />
            </View>
        );
    }
}
const styles = {
    pickerContainer: {
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 20,
        marginRight: 20,
        width: "80%",
        alignSelf: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#c5c5c5'
    }

};
export default DatePickerComponent;