import React, { Component } from 'react';
import { images } from '../../../assets';
import { Text, View } from 'react-native';
import CardItemList from '../CardComponent/index'
import { CardItem, Thumbnail, Card, Icon, Left, Body, Right, Accordion } from 'native-base';


const OrderView = (props) => {
  return (
    <Card>
      <CardItem style={{ padding: 0, }}>
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row', flex: 1 }}>
            <View style={{ marginRight: 5 }}>
              <Thumbnail source={images.notificationPoint} square small={true} />
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ margin: 5, fontWeight: 'bold' }}>Order # {props.arr.order_no}  </Text>
            </View>
          </View>
          <CardItemList title="Status" value={props.arr.status.split('_').join(" ")} />
          <CardItemList title="Payment Status" value={props.arr.payment_status} />
          <CardItemList title="Total Amount" value={props.arr.sub_total} />
        </View>
      </CardItem >
    </Card>
  )
};

class OrderViewDetail extends React.Component {
  _renderContent(item) {
    return (
      item.content
    );
  }

  render() {

    let dataArray = [
      {
        title: 'Customer Detail',
        content: <Card>
          <CardItem >
            <View style={{ flex: 1 }}>
              <CardItemList title="Customer Name" value={this.props.data.customer.name} />
              <CardItemList title="Customer Email" value={this.props.data.customer.email} />
              <CardItemList title="City" value={this.props.data.customer.city ? this.props.data.customer.city : "Not available"} />
              <CardItemList title="Country" value={this.props.data.customer.country ? this.props.data.customer.country : "Not available"} />
              <CardItemList title="State" value={this.props.data.customer.state ? this.props.data.customer.state : "Not available"} />
              <CardItemList title="Postal Code" value={this.props.data.customer.post_code ? this.props.data.customer.post_code : "Not available"} />
              <CardItemList title="Phone no" value={this.props.data.customer.phone ? this.props.data.customer.phone : "Not available"} />
            </View>
          </CardItem >
        </Card>
      },
      {
        title: 'Order Item',
        content: this.props.data.order_items.map((key, index) =>
          <View key={index}>
            <View key={"2" + index} style={{ flexDirection: 'row', flex: 1 }}>
              <View key={"3" + index} style={{ marginRight: 5, justifyContent: 'center' }}>
                <Thumbnail key={index} source={{ uri: "https:" + key.product.image }} square large={true} />
              </View>
              <View key={"4" + index} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text key={index} style={{ margin: 5, fontWeight: 'bold' }}>Name {key.product.name ? key.product.name : "Not available"}</Text>
              </View>
            </View>
            <CardItemList key={"c1" + index} title="SKU" value={key.product.sku ? key.product.sku : "Not available"} />
            <CardItemList key={"c2" + index} title="Quantity" value={key.quantity ? key.quantity : "Not available"} />
            <CardItemList key={"c3" + index} title="Price" value={key.total ? key.total : "Not available"} />
          </View>

        )
      },
      {
        title: 'Specifications',
        content: this.props.data.order_items.map((key, ind) =>
          <View key={ind}>
            {key.item_options.map((itemoption_key, index) =>
              <Card key={index}>
                <CardItem key={index} >
                  <View key={"1" + index}>
                    <CardItemList key={"c1" + index} title="Option" value={itemoption_key.option ? itemoption_key.option : "Not available"} />
                    <CardItemList key={"c2" + index} title="Option Value" value={itemoption_key.option_value ? itemoption_key.option_value : "Not available"} />
                  </View>
                </CardItem>
              </Card>
            )}
          </View>
        )
      },
      {
        title: 'Variant',
        content: this.props.data.order_items.map((key, index) =>
          <View key={index}>
            <Card key={index}>
              <CardItem key={index} >
                <View key={"1" + index}>
                  <View key={"2" + index} style={{ flexDirection: 'row', flex: 1 }}>
                    <View key={"3" + index} style={{ marginRight: 5 }}>
                      <Thumbnail key={index} source={{ uri: "https:" + key.variant.image }} square large={true} />
                    </View>
                    <View key={"4" + index} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text key={index} style={{ margin: 5, fontWeight: 'bold' }}>Name {key.variant.name ? key.variant.name : "Not available"}</Text>
                    </View>
                  </View>
                  <CardItemList key={"c1" + index} title="Production Time" value={key.variant.production_time ? key.variant.production_time : "Not available"} />
                </View>
              </CardItem>
            </Card>
          </View>
        )
      },


    ]





    return (
      <View>
        <Accordion
          dataArray={dataArray}
          animation={true}
          renderContent={this._renderContent}
          headerStyle={{ paddingTop: 15, paddingBottom: 15, margin: 3, borderBottomWidth: 1 }}
          expanded={0} />

        {/* <View style={{ flexDirection: 'row', flex: 1 }}>
            <View style={{ marginRight: 5 }}>
              <Thumbnail source={images.notificationPoint} square small={true} />
            </View>
            <Body>
              <Text style={{ margin: 5, fontWeight: 'bold' }}>Order # {this.props.arr.order_no}  </Text>
            </Body>
          </View> */}

      </View>
    )
  }
}
export {
  OrderView,
  OrderViewDetail
};