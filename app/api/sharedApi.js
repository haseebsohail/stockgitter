import { AsyncStorage } from 'react-native';
import { GetToken } from '../utils/helper'

// export const baseUrl = "http://d54f02dc.ngrok.io/mobile_api/v1";
// export const baseUrl = "http://192.168.88.247:3001/mobile_api/v1";
// "http://localhost:3000/mobile_api/v1/sign_in"
export const baseUrl = "https://api.stockgitter.com";
export let ENV = "USD"
export let JMD = "JMD"
export let header = async () => ({

	"Content-Type": "application/json",
	"Authorization": "Bearer " + await GetToken(),
	"Access-Control-Allow-Origin": "*"
});

export let authHeader = {
	"Content-Type": "application/json",
	"Access-Control-Allow-Origin": "*"
}
